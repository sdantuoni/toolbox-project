const admin = require('firebase-admin');
var serviceAccount = require('./cfg/firebase/[JSON_AUTH]');

var fireBase =  {};

fireBase.init = function ()
{
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount)
    });
    
    admin.firestore().settings( {
        timestampsInSnapshots: true
    });
}

module.exports = fireBase;
