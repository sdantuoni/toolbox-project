# ToolBox Back-end test  

Hi! This is a back-end demostrative proyect for **ToolBox**. If you want to run this proyect see the follow instructions.


# How to run it?

It is very simple.

> **Install Dependencies**
>  Run `npm install`

> **Run the API**
>  Run `npm start`
>  If you want to set any port run `PORT=[YOUR_PORT] npm start`


## Use the API
`/` Returns the API landing
`toolbox/:text` Returns the text in the parameter