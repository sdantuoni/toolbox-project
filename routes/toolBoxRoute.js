var express = require('express');
var router = express.Router();
var toolBoxModel = require('../models/toolBoxModel.js');

router.get('/toolbox/:text', function(req, res) {
    toolBoxModel.returnText(req, function(code, data)
    {
          res.status(code).json(data);
    });
});

module.exports = router;
