var express = require("express");
var router = express.Router();
var bodyParser  = require("body-parser");
var app = express();
var issues = require("./routes/toolBoxRoute");
var cors = require('cors');

router.get('/', function(request, response) {
   response.status(200).json({"message":"Welcome to NodeJS API Generator By @sdantuoni"});
});

app.use(bodyParser.json());
app.use(cors());

app.use(router);
app.use(issues);

const port = process.env.PORT || 3000;

app.listen(port, function() {
console.log("Yeah! API Generator By @sdantuoni is runing 🚀");
});

module.exports = app;
